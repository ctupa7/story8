from django.urls import path
from .views import index, data

appname = 'book'

urlpatterns = [
   path('', index, name = "find_books"),
   path('data/', data, name ="books_data"),
]
