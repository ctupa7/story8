$(document).ready(function(){
    var input = document.getElementById("input");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("btn").click();
        }
    });
    $("#btn").click(function(){
        var q = document.getElementById("input").value.toLowerCase();
        console.log(q);
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            success: function(data){
                $('#hasil').html('');
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='container-img' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='container-part'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='container-part'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='container-part'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='container-part'>" + data.items[i].volumeInfo.publishedDate + "</td>";
                }
                $('#hasil').append(result);
                document.getElementById("status").innerHTML = "";
            },
            error: (error) => {
                console.log(error)
                document.getElementById("status").innerHTML = "Book not found :("
            }
        })
    })
})