# Story-8 PPW

Repository ini merupakan hasil pengerjaan story-8 untuk mata kuliah Perancangan & Pemrograman Web.

## Heroku

Klik [disini](https://ctupa7-story8.herokuapp.com/) untuk melihat website.

## Pipeline and Coverage

[![pipeline status](https://gitlab.com/ctupa7/story8/badges/master/pipeline.svg)](https://gitlab.com/ctupa7/story8/commits/master)
[![coverage report](https://gitlab.com/ctupa7/story8/badges/master/coverage.svg)](https://gitlab.com/ctupa7/story8/commits/master)